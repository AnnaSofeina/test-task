/**
 * Test API
 */

//Подключаем dev-dependencies
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var expect = chai.expect;
var assert = chai.assert;

chai.use(chaiHttp); // используем http
var baseUrl = 'http://localhost:8001';

//глобальные тестовые переменные и данные
var validUserId = [];
var notValidUsersId = [];
var testUser = {
    username: "testUser",
    email: 'test@mail.ru',
    password: 'qwertyp0p',
    company: 'testCompany'
};

describe('GET users', () => {

    it('Получить всех пользователей', (done) => {
        chai.request(baseUrl)
            .get('/users')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
    });

});


describe('POST users', () => {

    it('Cоздание валидного пользователя со всеми полями', (done) => {
        chai.request(baseUrl)
            .post('/users')
            .send(testUser)
            .end((err, res) => {
                res.should.have.status(201); // создано
                res.should.be.a('object');
                res.body.should.have.property('id');
                res.body.should.have.property('username');
                res.body.should.have.property('email');
                res.body.should.have.property('password');
                res.body.should.have.property('company');
                testUser.id = res.body['id'];
                expect(res.body).to.eql(testUser);
                validUserId.push = res.body['id'];
                done();
            });
    });
    // Должен ли он возвращать пустое значение?
    it('Cоздание валидного пользователя без необязательного поля company', (done) => {
        var testUser_without_company = {
            username: "Егор",
            email: 'егор@ya.ru"',
            password: 'егорушка1'
        };
        chai.request(baseUrl)
            .post('/users')
            .send(testUser_without_company)
            .end((err, res) => {
                res.should.have.status(201); // создано
                res.should.be.a('object');
                res.body.should.have.property('id');
                res.body.should.have.property('username');
                res.body.should.have.property('email');
                res.body.should.have.property('password');

                testUser_without_company.id = res.body['id'];

                expect(res.body).to.eql(testUser_without_company);
                validUserId.push = res.body['id'];
                done();
            });
    });
    it('Cоздание невалидного пользователя (отправляем пустой массив данных)', (done) => {
        chai.request(baseUrl)
            .post('/users')
            .send({})
            .end((err, res) => {
                if(res.body && res.body['id']){
                    notValidUsersId.push(res.body['id']);
                }
                assert(res.status !== 201, 'Ошибка, сервер принимает пустой POST запрос без обязательных данных');
                done();
            });
    });
    it('Cоздание невалидного пользователя у которого отсутствует обязательное поле password', (done) => {
        var testUser_without_password = {
            username: "Alex",
            email: 'al@google.com',
        };
        chai.request(baseUrl)
            .post('/users')
            .send(testUser_without_password)
            .end((err, res) => {
                if(res.body && res.body['id']){
                    notValidUsersId.push(res.body['id']);
                }
                assert(res.status !== 201, 'Ошибка, сервер принимает POST запрос у которого отсутствует обязательное поле password');
                done();
            });
    });
    it('Получение ошибки валидации email при создании пользователя', (done) => {
        var testUser_incorrent_email = {
            username: "Maria Vasil'evna",
            email: 'maria',
            password: 'maria077'
        };
        chai.request(baseUrl)
            .post('/users')
            .send(testUser_incorrent_email)
            .end((err, res) => {
                if(res.body && res.body['id']){
                    notValidUsersId.push(res.body['id']);
                }
                assert(res.status !== 201, 'Ошибка, сервер принимает  POST запрос с невалидным форматом email');
                done();
            });
    });

});

describe('GET /users/:id', () => {

    it('Получить существующего пользователя по id', (done) => {
        chai.request(baseUrl)
            .get('/users/' + testUser.id)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');

                expect(res.body).to.eql(testUser);

                assert.isNumber(res.body['id'], 'Id должен быть int');
                assert.isString(res.body['username'], 'username должен быть string');
                assert.isString(res.body['email'], 'email должен быть string');
                assert.isString(res.body['password'], 'password должен быть string');
                assert.isString(res.body['company'], 'company должен быть string');
                done();
            });
    });
    it('Получить 404 статус пользователя которого нет (id=-1)', (done) => {
        chai.request(baseUrl)
            .get('/users/-1')
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
    });
    it('Получить существующего пользователя по параметру username', (done) => {
        chai.request(baseUrl)
            .get('/users?username=' + testUser.username + '&&id=' + testUser.id)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                expect(res.body[0]).to.eql(testUser); // потому что пришел в array
                done();
            });
    });
    it('Получить несуществующего пользователя по параметру company', (done) => {
        chai.request(baseUrl)
            .get('/users?company=R')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
    });
    it('Получить пользователя по несуществующему полю', (done) => {
        chai.request(baseUrl)
            .get('/users?pasport=45005678')
            .end((err, res) => {
                assert(res.should.status !== 200, 'Мы получили пользователя, который не может иметь такого параметра');
                done();
            });
    });

});

describe('PATCH /users/:id', () => {

    it('Редактирование поле company у существующего пользователя', (done) => {
        chai.request(baseUrl)
            .put('/users/' + testUser.id)
            .send({username: "The Chronicles of Narnia"})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('username').eql('The Chronicles of Narnia');
                done();
            });
    });
    it('Редактирование у пользователя поля password на невалидное значение', (done) => {
        chai.request(baseUrl)
            .put('/users/' + testUser.id)
            .send({password: "!!!"})
            .end((err, res) => {
                assert(res.status !== 201, 'Ошибка, PATCH принимает на невалидное значение password');
                done();
            });
    });

});

describe('DELETE /users/:id', () => {

    it('Удалить созданных валидных пользователей (по id)', (done) => {
        for (var i = 0; i < validUserId.length; i++) {
            chai.request(baseUrl)
                .del('/users/' + validUserId[i])
                .end((err, res) => {
                    res.should.have.status(200);
                });
        }
        done();
    });

    it('Удалить созданных невалидных пользователей (по id)', (done) => {
        for (var i = 0; i < notValidUsersId.length; i++) {
            chai.request(baseUrl)
                .del('/users/' + notValidUsersId[i])
                .end((err, res) => {
                    res.should.have.status(200);
                });
        }
        done();
    });

    it('Удалить несуществующего пользователя (по id), должен выдать ошибку 404', (done) => {
        chai.request(baseUrl)
            .delete('/users/-2')
            .end((err, res) => {
                res.should.have.status(404);
                done();
            });
    });

});
